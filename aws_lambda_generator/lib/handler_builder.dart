import 'dart:async';
import 'dart:developer' as dev;
import 'dart:io';

import 'package:analyzer/dart/element/element.dart';
import 'package:aws_lambda/aws_lambda.dart';
import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';

Builder prepareHandler(BuilderOptions options) {
  return AwsHandlerMainBuilder();
}

class AwsHandlerMainBuilder extends Builder {
  final _runtimes = <String>[];

  final mainGenerator = AwsHandlerMainGenerator();
  final importGenerator = AwsHandlerImportGenerator();

  AwsHandlerMainBuilder() {}

  @override
  FutureOr<void> build(BuildStep buildStep) async {
    var importLines = (await importGenerator.generateForStep(buildStep));
    if (importLines.length > 0) {
      var registerLines = (await mainGenerator.generateForStep(buildStep));
      var fileName = buildStep.inputId.pathSegments;
      var lastSegment = fileName.removeLast();
      lastSegment = lastSegment.substring(0, lastSegment.lastIndexOf("."));
      fileName.add("$lastSegment.main");
      dev.log("Generating file: $fileName");
      File(fileName.join("/"))
          .writeAsStringSync(_generateOutput(importLines, registerLines));

//      buildStep.writeAsString(
//          mainHandlerAsset, _generateOutput(importLines, registerLines));
      // prepare AOT build marker

//      File("")
    }
  }

  @override
  Map<String, List<String>> get buildExtensions =>
      {
        '.dart': ['.main.dart']
      };

  FutureOr<String> _generateOutput(String importLines, String registerLines) {
    return """
import 'package:aws_lambda/aws_lambda.dart';
$importLines
/// generated at ${DateTime.now()}
void main() async {
  await Runtime()
$registerLines
    ..start();
}
""";
  }
}

abstract class AwsHandlerGenerator extends GeneratorForAnnotation<AwsHandler> {
  Future<LibraryReader> getReader(BuildStep buildStep) async {
    final library = await buildStep.inputLibrary;
    return LibraryReader(library);
  }

  AwsHandler parseHandler(ConstantReader annotation, {Element element}) {
    var nameValue = annotation.read("name");
    return AwsHandler(
        name: nameValue.isNull ? element.name : nameValue.stringValue,
        runtime: annotation
            .read("runtime")
            .stringValue);
  }

  FutureOr<String> generateForStep(BuildStep buildStep) async {
    return generate(await getReader(buildStep), buildStep);
  }
}

class AwsHandlerImportGenerator extends AwsHandlerGenerator {
  @override
  FutureOr<String> generateForAnnotatedElement(Element element,
      ConstantReader annotation, BuildStep buildStep) async {
    return """
import '${buildStep.inputId.pathSegments.last}'; 
""";
  }
}

class AwsHandlerMainGenerator extends AwsHandlerGenerator {
  @override
  FutureOr<String> generateForAnnotatedElement(Element element,
      ConstantReader annotation, BuildStep buildStep) async {
    var awsLambda = parseHandler(annotation, element: element);
    if (element is FunctionElement) {
      var handlerType = element.parameters.first.type.name;
      print(
          "Processing AWS Lambda for element: $awsLambda - ${element
              .runtimeType} / ${handlerType} / ${element.parameters}");
      if (handlerType == "AwsCognitoEvent") {
        return """    ..registerCognitoHandler("${awsLambda.name}", ${element
            .name})""";
      } else if (handlerType == "ApiGatewayEvent") {
        return """    ..registerApiGatewayHandler("${awsLambda.name}", ${element
            .name})""";
      } else if (handlerType == "AwsS3NotificationEvent") {
        return """    ..registerS3Handler("${awsLambda.name}", ${element
            .name})""";
      } else {
        return """    ..registerHandler("${awsLambda.name}", ${element
            .name})""";
      }
    } else {
      return "// no supported Handler for ${awsLambda.name}";
    }
  }
}
