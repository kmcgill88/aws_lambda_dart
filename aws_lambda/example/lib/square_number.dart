import 'dart:convert';
import 'dart:math';

import 'package:aws_lambda/aws_lambda.dart';
import 'package:aws_lambda/runtime/s3_event.dart';

@AwsHandler(name: "squareNumber")
Future<AwsLambdaResponse> squareNumber(
    AwsLambdaRequest request, Context context) async {
  final number = request.body['number'];
  var squareNumber = number * number;
  return AwsLambdaResponse({'result': squareNumber});
}

@AwsHandler()
Future<AwsLambdaResponse> rootNumber(
    ApiGatewayEvent event, Context context) async {
  final number = jsonDecode(event.body)['number'];
  var rootNumber = sqrt(number);
  return AwsLambdaResponse({'result': rootNumber});
}

@AwsHandler(name: "sizeInMegaBytes")
Future<AwsLambdaResponse> sizeInMegaBytes(
    AwsS3NotificationEvent event, Context context) async {
  final number = event.records.first.s3.object.size;
  var squareNumber = number / 1024 / 1024;
  return AwsLambdaResponse({'result': squareNumber});
}

@AwsHandler(name: "signUp")
Future<AwsLambdaResponse> signUpCognito(
    AwsCognitoEvent event, Context context) async {
  event.response.autoConfirmUser = true;
  return AwsLambdaResponse(event.toJson());
}
