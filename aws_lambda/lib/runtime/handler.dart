class AwsHandler {
  final String runtime;
  final String name;

  const AwsHandler({this.name, this.runtime = "main"});

  @override
  String toString() {
    return "AwsHandler('$runtime.$name')";
  }
}

/// Generic type for AWS Lambda request with body and metadata parameters
class AwsLambdaRequest {
  final Map<String, dynamic> body;
  final Map<String, dynamic> metadata;

  AwsLambdaRequest(this.body, {this.metadata = const {}});

  factory AwsLambdaRequest.withMetadata(Map<String, dynamic> body,
      Map<String, dynamic> metadata) {
    return AwsLambdaRequest(body, metadata: metadata);
  }
}

/// Generic type fo AWS Lambda response with body and metadata
class AwsLambdaResponse {
  final Map<String, dynamic> body;
  final Map<String, dynamic> metadata;

  AwsLambdaResponse(this.body, {this.metadata = const {}});

  factory AwsLambdaResponse.withMetadata(Map<String, dynamic> body,
      Map<String, dynamic> metadata) {
    return AwsLambdaResponse(body, metadata: metadata);
  }
}
