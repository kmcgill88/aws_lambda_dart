// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cognito_event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AwsCognitoEvent _$AwsCognitoEventFromJson(Map<String, dynamic> json) {
  return AwsCognitoEvent()
    ..version = json['version'] as String
    ..triggerSource = json['triggerSource'] as String
    ..region = json['region'] as String
    ..userPoolId = json['userPoolId'] as String
    ..userName = json['userName'] as String
    ..callerContext = json['callerContext'] == null
        ? null
        : AwsCognitoCallerContext.fromJson(
            json['callerContext'] as Map<String, dynamic>)
    ..request = json['request'] == null
        ? null
        : AwsCognitoRequest.fromJson(json['request'] as Map<String, dynamic>)
    ..response = json['response'] == null
        ? null
        : AwsCognitoResponse.fromJson(json['response'] as Map<String, dynamic>);
}

Map<String, dynamic> _$AwsCognitoEventToJson(AwsCognitoEvent instance) =>
    <String, dynamic>{
      'version': instance.version,
      'triggerSource': instance.triggerSource,
      'region': instance.region,
      'userPoolId': instance.userPoolId,
      'userName': instance.userName,
      'callerContext': instance.callerContext,
      'request': instance.request,
      'response': instance.response,
    };

AwsCognitoCallerContext _$AwsCognitoCallerContextFromJson(
    Map<String, dynamic> json) {
  return AwsCognitoCallerContext()
    ..awsSdkVersion = json['awsSdkVersion'] as String
    ..clientId = json['clientId'] as String;
}

Map<String, dynamic> _$AwsCognitoCallerContextToJson(
        AwsCognitoCallerContext instance) =>
    <String, dynamic>{
      'awsSdkVersion': instance.awsSdkVersion,
      'clientId': instance.clientId,
    };

AwsCognitoRequest _$AwsCognitoRequestFromJson(Map<String, dynamic> json) {
  return AwsCognitoRequest()
    ..userAttributes = (json['userAttributes'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, e as String),
    )
    ..validationData = (json['validationData'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, e as String),
    )
    ..clientMetadata = (json['clientMetadata'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, e as String),
    )
    ..newDeviceUsed = json['newDeviceUsed'] as bool;
}

Map<String, dynamic> _$AwsCognitoRequestToJson(AwsCognitoRequest instance) =>
    <String, dynamic>{
      'userAttributes': instance.userAttributes,
      'validationData': instance.validationData,
      'clientMetadata': instance.clientMetadata,
      'newDeviceUsed': instance.newDeviceUsed,
    };

AwsCognitoResponse _$AwsCognitoResponseFromJson(Map<String, dynamic> json) {
  return AwsCognitoResponse()
    ..autoConfirmUser = json['autoConfirmUser'] as bool
    ..autoVerifyPhone = json['autoVerifyPhone'] as bool
    ..autoVerifyEmail = json['autoVerifyEmail'] as bool;
}

Map<String, dynamic> _$AwsCognitoResponseToJson(AwsCognitoResponse instance) =>
    <String, dynamic>{
      'autoConfirmUser': instance.autoConfirmUser,
      'autoVerifyPhone': instance.autoVerifyPhone,
      'autoVerifyEmail': instance.autoVerifyEmail,
    };

AwsGroupConfiguration _$AwsGroupConfigurationFromJson(
    Map<String, dynamic> json) {
  return AwsGroupConfiguration()
    ..groupsToOverride =
    (json['groupsToOverride'] as List)?.map((e) => e as String)?.toList()
    ..iamRolesToOverride =
    (json['iamRolesToOverride'] as List)?.map((e) => e as String)?.toList()
    ..preferredRole = json['preferredRole'] as String
    ..clientMetadata = (json['clientMetadata'] as Map<String, dynamic>)?.map(
          (k, e) => MapEntry(k, e as String),
    );
}

Map<String, dynamic> _$AwsGroupConfigurationToJson(
    AwsGroupConfiguration instance) =>
    <String, dynamic>{
      'groupsToOverride': instance.groupsToOverride,
      'iamRolesToOverride': instance.iamRolesToOverride,
      'preferredRole': instance.preferredRole,
      'clientMetadata': instance.clientMetadata,
    };

AwsClaimOverrideDetails _$AwsClaimOverrideDetailsFromJson(
    Map<String, dynamic> json) {
  return AwsClaimOverrideDetails()
    ..claimsToAddOrOverride =
    (json['claimsToAddOrOverride'] as Map<String, dynamic>)?.map(
          (k, e) => MapEntry(k, e as String),
    )
    ..claimsToSuppress =
    (json['claimsToSuppress'] as List)?.map((e) => e as String)?.toList()
    ..groupOverrideDetails = json['groupOverrideDetails'] == null
        ? null
        : AwsGroupConfiguration.fromJson(
        json['groupOverrideDetails'] as Map<String, dynamic>);
}

Map<String, dynamic> _$AwsClaimOverrideDetailsToJson(
    AwsClaimOverrideDetails instance) =>
    <String, dynamic>{
      'claimsToAddOrOverride': instance.claimsToAddOrOverride,
      'claimsToSuppress': instance.claimsToSuppress,
      'groupOverrideDetails': instance.groupOverrideDetails,
    };
